package com.dashboard.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.dashboard.entity.DeliverySummaryModel;
import com.dashboard.service.DeliverySummaryService;

@RestController
@CrossOrigin(methods = { RequestMethod.GET,
		RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT })
public class DeliverySummaryController {

	@Autowired
	DeliverySummaryService deliverySummaryService;
	
	@PostMapping(path = "/create")
	public ResponseEntity<String> saveSummary(@RequestBody DeliverySummaryModel deliverySummaryModel) {
		deliverySummaryService.saveOrUpdate(deliverySummaryModel);
		return new ResponseEntity<String>("record inserted successfully", HttpStatus.OK);
	}

	@GetMapping(path = "/get-summary/{sprintId}/{projectId}")
	public ResponseEntity<DeliverySummaryModel> getSummaryBySprint(@PathVariable("sprintId") long sprintId, @PathVariable("projectId")long projectId) throws Exception {
		DeliverySummaryModel response = deliverySummaryService.getDeliverySummaryBySprint(sprintId, projectId);
		return new ResponseEntity<DeliverySummaryModel>(response, HttpStatus.OK);
	}
	@GetMapping("/summary")
	public ResponseEntity<List<DeliverySummaryModel>> getSummaryDetails() {
		List<DeliverySummaryModel> response = deliverySummaryService.getDeliverySummary();
		return new ResponseEntity<>(
				response,
				HttpStatus.OK);
	}

}
