package com.dashboard.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dashboard.entity.DeliverySummaryModel;
import com.dashboard.repository.DeliverySummaryRepository;


@Service
public class DeliverySummaryService {

	@Autowired
	DeliverySummaryRepository repository;
	
	public void saveOrUpdate(DeliverySummaryModel dsmodel) {
		repository.save(dsmodel);
	}

	public List<DeliverySummaryModel> getDeliverySummary() {
		List<DeliverySummaryModel> response= repository.findAll();
		return response;
	}

	public DeliverySummaryModel getDeliverySummaryBySprint(long sprintId, long projectId) throws Exception {
		Optional<DeliverySummaryModel> deliverySummaryDetails = repository.findBySprintIdAndProjectId(sprintId, projectId);
		if(deliverySummaryDetails.isPresent()){
			return deliverySummaryDetails.get();
		}else{
			throw new Exception("No details found");
		}
	}

	/*public void deleteDeliverySummaryBySprint(int sprintId) {
		repository.deleteById(sprintId);;
	}*/
	
}
