package com.dashboard.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "delivery_summary", schema = "delivery_summary")
public class DeliverySummaryModel {

	@Column
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Id
	private long id;
	
	@Column
	private long sprintId;
	
	@Column
	private long projectId;
	
	@Column
	private String offsitepm;
	
	@Column
	private String onsitepm;
	
	@Column
	private int capacity;
	
	@Column
	private int velocity;
	
	@Column
	private int completedStoryPoints;
	
	@Column
	private int plannedStoryPoints;
	
	@Column
	private String deliveryLeadName;
	
	@Column
	private String portfolioLeadName;
	
	@Column
	private String featuresDelivered;
	
	@Column
	private String businessValue;
	
	@Column
	private String positives;
	
	@Column
	private String scopeForImprovements;
	
	@Column
	private String issuesOrRisks;
	
	@Column
	private String impact;
	
	@Column
	private String needLeadershipAttention;
	
	@Column
	private String owner;
	
	@Column
	private String mitigation;
	
	@Column
	private String actionTaken;

	@Column
	private String schedule;

	@Column
	private String estimation;

	@Column
	private String resources;

	@Column
	private String customerPulse;

	
}
