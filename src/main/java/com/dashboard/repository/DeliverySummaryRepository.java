package com.dashboard.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.dashboard.entity.DeliverySummaryModel;

public interface DeliverySummaryRepository extends JpaRepository<DeliverySummaryModel, Long>{

	Optional<DeliverySummaryModel> findBySprintIdAndProjectId(long sprintName, long projectName);

}
