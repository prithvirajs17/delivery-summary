package com.dashboard.service;

import com.dashboard.entity.DeliverySummaryModel;
import com.dashboard.repository.DeliverySummaryRepository;
import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class DeliverySummaryServiceTest {

    @InjectMocks
    DeliverySummaryService service;

    @Mock
    DeliverySummaryRepository repo;

    @Test
    public void saveOrUpdateTest(){
        DeliverySummaryModel request = new DeliverySummaryModel();
        service.saveOrUpdate(request);
    }

    @Test
    public void getDeliverySummaryTest(){
        List<DeliverySummaryModel> response =  new ArrayList<>();
        DeliverySummaryModel deliverySummaryModel = new DeliverySummaryModel();
        deliverySummaryModel.setOffsitepm("Prithvi");
        response.add(deliverySummaryModel);
        Mockito.when(repo.findAll()).thenReturn(response);
        List<DeliverySummaryModel> result = service.getDeliverySummary();
        assertNotNull(result.get(0));
    }

    @Test
    public void getDeliverySummaryBySprintTest() throws Exception {
        Optional<DeliverySummaryModel> response = Optional.of(new DeliverySummaryModel());
        response.get().setOffsitepm("username");
        response.get().setProjectId(12);
        response.get().setSprintId(12);
        Mockito.when(repo.findBySprintIdAndProjectId(12,12)).thenReturn(response);
        DeliverySummaryModel result = service.getDeliverySummaryBySprint(12,12);
        assertTrue("username".equals(result.getOffsitepm()));
    }

    @Test
    public void getDeliverySummaryBySprintExceptionTest() throws Exception {
        Optional<DeliverySummaryModel> response;
        try{
            DeliverySummaryModel res = service.getDeliverySummaryBySprint(12,12);
        }catch (Exception ex){
            assertNotNull(ex.getMessage());
        }
    }
}
