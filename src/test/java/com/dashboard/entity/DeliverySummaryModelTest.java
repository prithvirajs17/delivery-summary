package com.dashboard.entity;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
public class DeliverySummaryModelTest {

    @Test
    public void test(){
        DeliverySummaryModel deliverySummaryModel = new DeliverySummaryModel();
        deliverySummaryModel.setSprintId(11);
        deliverySummaryModel.setProjectId(11);
        deliverySummaryModel.setOffsitepm("user");
        deliverySummaryModel.setOnsitepm("user1");
        deliverySummaryModel.setVelocity(8);
        deliverySummaryModel.setCapacity(8);
        deliverySummaryModel.setEstimation("green");
    }

    @Test
    public void testToString() {
        DeliverySummaryModel deliverySummaryModel = new DeliverySummaryModel();
        String result = "DeliverySummaryModel(id=null, sprintId=null, projectId=null, offsitepm=null, onsitepm=null, capacity=null, velocity=null, completedStoryPoints=null, plannedStoryPoints=null, deliveryLeadName=null, portfolioLeadName=null, featuresDelivered=null)";
        assertEquals(deliverySummaryModel.toString(), result);
    }

    @Test
    public void testHashcode() {
        DeliverySummaryModel p1 = new DeliverySummaryModel();
        DeliverySummaryModel p2 = new DeliverySummaryModel();
        Map<DeliverySummaryModel, String> map = new HashMap<>();
        map.put(p1, "dummy");
        assertEquals("dummy", map.get(p2));
    }
}
