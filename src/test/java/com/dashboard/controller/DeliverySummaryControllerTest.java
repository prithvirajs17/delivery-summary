package com.dashboard.controller;

import com.dashboard.entity.DeliverySummaryModel;
import com.dashboard.service.DeliverySummaryService;
import org.junit.Test;
//import org.springframework.test.util.ReflectionTestUtils;
import org.junit.runner.RunWith;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.powermock.api.mockito.PowerMockito.when;

@ExtendWith(MockitoExtension.class)
public class DeliverySummaryControllerTest {
    @Mock
    DeliverySummaryService service;
    @InjectMocks
    DeliverySummaryController controller;

    @Test
    public void testSaveSummary(){
        DeliverySummaryModel deliverySummaryModel = new DeliverySummaryModel();
        deliverySummaryModel.setId(1);
        deliverySummaryModel.setSprintId(12);
        deliverySummaryModel.setProjectId(12);
        deliverySummaryModel.setCapacity(4);
        deliverySummaryModel.setVelocity(5);

        ResponseEntity<String> response = controller.saveSummary(deliverySummaryModel);
        assertTrue(response.getStatusCode() == HttpStatus.OK);
        assertTrue((response.getBody()) != null);
    }

    @Test
    public void testGetSummaryBySprint() throws Exception {
        DeliverySummaryModel response = new DeliverySummaryModel();
        response.setOffsitepm("Prithvi");
        when(service.getDeliverySummaryBySprint(11,11)).thenReturn(response);
        ResponseEntity<DeliverySummaryModel> res = controller.getSummaryBySprint(11,11);
        assertTrue(res.getBody().getOffsitepm().equals("Prithvi"));
    }

    public void testGetSummaryDetails() {
        List<DeliverySummaryModel> response = new ArrayList<>();
        DeliverySummaryModel deliverySummaryResponse = new DeliverySummaryModel();
        deliverySummaryResponse.setOffsitepm("Prithvi");
        response.add(deliverySummaryResponse);
        when(service.getDeliverySummary()).thenReturn(response);
        ResponseEntity<List<DeliverySummaryModel>> res = controller.getSummaryDetails();
        assertTrue(res.getBody().get(0).getOffsitepm().equals(response.get(0).getOffsitepm()));
    }

}
